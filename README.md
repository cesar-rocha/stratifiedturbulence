# README #

This repository contains a python solver for the 2D-XZ Boussinesq equations
with uniform stratification.
The code calculates both non-linear and quasi-linear solutions.

### Requirements ###
The code is written in Python 3 and requires standard
python packages for scientific computing:

* Numpy
* Scipy
* h5py

To speed up FFT computations, I recommend installing [mklfft](https://github.com/IntelPython/mkl_fft).

### Usage ###
The source code is inside`simulations/`. The control file is params.py. For test runs, launch ``ipython`` and type 
```{ipython}
run run_model.py
```
For serious long runs, use
```{bash}
    nohup python run_model.py > simulation.log &
```
