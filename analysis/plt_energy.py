import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['lines.linewidth'] = 2.5

ap = .65
plt.style.use('seaborn-deep')

plt.close('all')

path = "../252/Frs/0.06/forced_ql"
fni = path+"/diags.npz"

diags = np.load(fni)

tN = diags['tN']
U0 = 1.
s  = U0**2/2/tN

ep = diags['hep'] + diags['ep']
chi = diags['hchi'] + diags['chi']

plt.figure(figsize=(8,5))

plt.subplot(111)

plt.plot(diags['time']/tN,diags['work']/s,label=r'$W$',alpha=ap)
plt.plot(diags['time']/tN,-ep/s,label=r'$-\varepsilon$',alpha=ap)
plt.plot(diags['time']/tN,-chi/s,label=r'$-\chi$',alpha=ap)
plt.plot(diags['time']/tN,diags['denergy']/s,color='y',label=r'$\dot{E}$',alpha=ap)
plt.plot(diags['time']/tN,diags['rhs']/s,'k--',linewidth=1.5,
        label=r'$W - \varepsilon -\chi$',alpha=ap)
plt.ylim(-0.5,.5)
plt.xlim(0,75)
plt.legend(loc=4,ncol=3)
plt.grid()
plt.xlabel(r'Time $[t\times N/2\pi]$')
plt.ylabel(r'Power $[\dot{E}\times  2\pi/(N \bar{E})]$')

#plt.savefig(path+"/denergy.pdf")
