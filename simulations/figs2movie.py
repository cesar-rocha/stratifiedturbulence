import glob
import h5py
import cmocean
import numpy as np
import matplotlib.pyplot as plt

plt.close("all")

__pathi__ = "new/nl/"
__patho__ = "figs2movie/"

diags = np.load(__pathi__+"diags.npz")
setup = h5py.File(__pathi__+"setup.h5") 

# grid
x, y = setup["grid/x"][:], setup['grid/y'][:]

# physical params
tN = diags['tN']
L = 2*np.pi/3

F = 0.02
Re4 = 25000

# normalization constants
Q = 2*np.pi
B = 2*np.pi 

# contour levels
cq = np.linspace(-1.5,1.5,40)

snapshots = glob.glob(__pathi__+"snapshots/*.h5")

for fni in snapshots:

    snap = h5py.File(fni)

    fig = plt.figure(figsize=(5,5))
    ax = fig.add_subplot(aspect=1)

    plt.contourf(x/L,y/L,snap['q'][:]/Q,cq,cmap=cmocean.cm.curl,extend='both') 
    plt.xlabel(r"$x\,m_0/2\pi$")
    plt.ylabel(r"$y\,m_0/2\pi$")
    plt.title(r" $t = %3.2f$" %(snap['t'][()]/tN))
    plt.savefig(__patho__+snap.filename[-18:-3]+".png",dpi=80)
    plt.close(fig)
