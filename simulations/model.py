import numpy as np
from numpy import pi
import logging
import h5py
from numpy import pi
import logging, os

class Model(object):

    """" two-dimensional x-z model """

    def __init__(
        self,
        # grid size parameters
        nx=64,                     # grid resolution
        ny=None,
        Lx=1e6,                     # domain size is L [m]
        Lz = 1e3,                   # domain size
        # timestepping parameters
        dt=7200.,                   # numerical timestep
        twrite=1000.,               # interval for cfl and ke writeout (in timesteps)
        tswrite=2,
        tmax=1576800000.,           # total time of integration
        tavestart=315360000.,       # start time for averaging
        taveint=86400.,             # time interval used for summation in longterm average in seconds
        tdiags=10,
        # friction parameters
        use_filter = True,
        # constants
        g= 9.81,                    # acceleration due to gravity
        N = 1e-3,                   # stratification frequency
        nu = 1.e-2,
        kappa = 1e-2,
        nu4 = 0,
        kappa4=0,
        Re=100,                     # Reynolds number
        Pr=1,                       # Prandtl number
        Fr = 0.1,                   # vertical Froude number
        r = 0.,                     # linear drag
        use_mkl=True,               # use mkl fft 
        nthreads=1,                  # number of threads for MKL fft computations
        ql = False,                 # If true, use QL dynamics; default (False) uses is NL dynamics
        dealias = False,
        save_snapshots=True,
        overwrite=True,
        tsave_snapshots=10,  # in snapshots
        path = 'output/'):

        # put all the parameters into the object
        # grid
        self.nx = nx
        self.ny = nx
        self.L = Lx
        self.H = Lz

        # timestepping
        self.dt = dt
        self.twrite = twrite
        self.tswrite = tswrite
        self.tmax = tmax
        self.tavestart = tavestart
        self.taveint = taveint
        self.tdiags = tdiags
        # fft
        self.dealias = dealias

        # quasilinear flag
        self.ql = ql

        # constants
        self.r = r
        self.g = g
        self.N = N
        self.N2 = N**2
        self.nu = nu
        self.nu4= nu4
        self.kappa = kappa
        self.kappa4= kappa4
        self.Re = Re
        self.Pr = Pr
        self.Fr = Fr
        self.Fr2 = Fr**2
        # saving stuff
        self.save_snapshots = save_snapshots
        self.overwrite = overwrite
        self.tsave_snapshots = tsave_snapshots
        self.path = path


        # flags
        self.use_filter = use_filter
        self.use_mkl = use_mkl

        # threads
        self.nthreads = nthreads


        self._initialize_logger()
        self._initialize_grid()
        self._allocate_variables()
        self._initialize_filter()
        self._init_etdrk4()
        self._initialize_time()
        self._initialize_diagnostics()

        # initialize path to save
        self._init_save_snapshots(self.path)
        self._save_setup()
        self.cflmax = .9

        # fft
        self._init_fft()
        #self._initialize_diagnostics()

    def _allocate_variables(self):
        """ Allocate variables in memory """

        self.dtype_real = np.dtype('float64')
        self.dtype_cplx = np.dtype('complex128')
        self.shape_real = (self.ny, self.nx)
        self.shape_cplx = (self.ny, self.nx)

        # vorticity
        self.q  = np.zeros(self.shape_real,  self.dtype_real)
        self.qh = np.zeros(self.shape_cplx,  self.dtype_cplx)

        # stream function
        self.p  = np.zeros(self.shape_real,  self.dtype_real)
        self.ph = np.zeros(self.shape_cplx,  self.dtype_cplx)

        # buoyancy function
        self.b  = np.zeros(self.shape_real,  self.dtype_real)
        self.bh = np.zeros(self.shape_cplx,  self.dtype_cplx)


    def run_with_snapshots(self, tsnapstart=0., tsnapint=432000.):
        """Run the model forward, yielding to user code at specified intervals.

        Parameters
        ----------

        tsnapstart : int
            The timestep at which to begin yielding.
        tstapint : int
            The interval at which to yield.
        """

        tsnapints = np.ceil(tsnapint/self.dt)

        while(self.t < self.tmax):
            self._step_forward()
            if self.t>=tsnapstart and (self.tc%tsnapints)==0:
                yield self.t
        return

    def run(self):
        """Run the model forward without stopping until the end."""
        while(self.t < self.tmax):
            self._step_forward()

    ### PRIVATE METHODS - not meant to be called by user ###

    def _step_forward(self):

        self._step_etdrk4()
        self._print_status()
        self._increment_diagnostics()
        self._save_snapshots()


    def _initialize_time(self):
        """Set up timestep stuff"""
        self.t=0        # actual time
        self.tc=0       # timestep number
        self.taveints = np.ceil(self.taveint/self.dt)

    ### initialization routines, only called once at the beginning ###
    def _initialize_grid(self):
        """Set up spatial and spectral grids and related constants"""
        self.x,self.y = np.meshgrid(
            np.arange(0.5,self.nx,1.)/self.nx*self.L,
            np.arange(0.5,self.ny,1.)/self.ny*self.H )

        self.dk = 2.*pi/self.L
        self.dl = 2.*pi/self.H

        # wavenumber grids
        self.nl = self.ny
        self.nk = self.nl
        self.ll = self.dl*np.append( np.arange(0.,self.nx//2),
            np.arange(-self.nx//2,0.) )
        #self.kk = self.dk*np.arange(0.,self.nx//2+1)
        self.kk = self.ll.copy()

        self.k, self.l = np.meshgrid(self.kk, self.ll)
        self.ki = 1j*self.k
        self.li = 1j*self.l

        # physical grid spacing
        self.dx = self.L / self.nx
        self.dy = self.H / self.ny

        # constant for spectral normalizations
        self.M = self.nx*self.ny

        # isotropic wavenumber^2 grid
        # the inversion is not defined at kappa = 0
        self.wv2 = self.k**2 + self.l**2
        self.wv = np.sqrt( self.wv2 )
        self.wv4 = self.wv2**2

        iwv2 = self.wv2 != 0.
        self.wv2i = np.zeros_like(self.wv2)
        self.wv2i[iwv2] = self.wv2[iwv2]**-1

    def _initialize_background(self):
        raise NotImplementedError(
            'needs to be implemented by Model subclass')

    def _initialize_inversion_matrix(self):
        raise NotImplementedError(
            'needs to be implemented by Model subclass')

    def _initialize_forcing(self):
        raise NotImplementedError(
            'needs to be implemented by Model subclass')

    def _initialize_filter(self):
        """Set up frictional filter."""
        # this defines the spectral filter (following Arbic and Flierl, 2003)

        if self.use_filter:
            cphi=0.65*pi
            wvx=np.sqrt((self.k*self.dx)**2.+(self.l*self.dy)**2.)
            self.filtr = np.exp(-23.6*(wvx-cphi)**4.)
            self.filtr[wvx<=cphi] = 1.
            self.logger.info(' Using filter')
        elif self.dealias:
            self.filtr = np.ones_like(self.wv2)
            self.filtr[self.nx/3:2*self.nx/3,:] = 0.
            self.filtr[:,self.ny/3:2*self.ny/3] = 0.
            self.logger.info(' Dealiasing with 2/3 rule')
        else:
            self.filtr = np.ones_like(self.wv2)
            self.logger.info(' No dealiasing; no filter')


    def _do_external_forcing(self):
        pass

    # logger
    def _initialize_logger(self):

        self.logger = logging.getLogger(__name__)

        fhandler = logging.StreamHandler()
        formatter = logging.Formatter('%(levelname)s: %(message)s')

        fhandler.setFormatter(formatter)

        if not self.logger.handlers:
            self.logger.addHandler(fhandler)

        self.log_level = 1
        self.logger.setLevel(self.log_level*10)

        # this prevents the logger from propagating into the ipython notebook log
        self.logger.propagate = False
        self.logger.info(' Logger initialized')


    def _step_etdrk4(self):
        """ march the system forward using a ETDRK4 scheme """

        # q-equation
        self.qh0 = self.qh.copy()
        Fn0 = -self.jacobian_psi_q() + self.ki*self.bh + self.fh
        self.qh = (self.expch_h*self.qh0 + Fn0*self.Qh)*self.filtr
        self.qh1 = self.qh.copy()

        # b-equation
        self.bh0 = self.bh.copy()
        Fn0b = -self.jacobian_psi_b() - self.ki*self.ph*self.N2
        self.bh = (self.expch_hb*self.bh0 + Fn0b*self.Qhb)*self.filtr
        self.bh1 = self.bh.copy()

        # q-equation
        self._invert()
        Fna = -self.jacobian_psi_q() + self.ki*self.bh + self.fh
        self.qh = (self.expch_h*self.qh0 + Fna*self.Qh)*self.filtr

        # b-equation
        Fnab = -self.jacobian_psi_b() - self.ki*self.ph*self.N2
        self.bh = (self.expch_hb*self.bh0 + Fnab*self.Qhb)*self.filtr

        # q-equation
        self._invert()
        Fnb = -self.jacobian_psi_q()+ self.ki*self.bh + self.fh
        self.qh = (self.expch_h*self.qh1 + ( 2.*Fnb - Fn0 )*self.Qh)*self.filtr

        # b-equation
        Fnbb = -self.jacobian_psi_b() - self.ki*self.ph*self.N2
        self.bh = (self.expch_hb*self.bh1 + ( 2.*Fnbb - Fn0b )*self.Qhb)*self.filtr

        # q-equation
        self._invert()
        Fnc = -self.jacobian_psi_q() + self.ki*self.bh + self.fh
        self.qh = (self.expch*self.qh0 + Fn0*self.f0 +  2.*(Fna+Fnb)*self.fab\
                  + Fnc*self.fc)*self.filtr

        # b-equation
        Fncb = -self.jacobian_psi_b()- self.ki*self.ph*self.N2
        self.bh = (self.expchb*self.bh0 + Fn0b*self.f0b +  2.*(Fnab+Fnbb)*self.fabb\
                  + Fncb*self.fcb)*self.filtr

        # calcuate q and b to save
        self._invert()
        self.u, self.v = self.ifft(-self.li*self.ph).real, self.ifft(self.ki*self.ph).real
        self.q = self.ifft(self.qh).real
        self.b = self.ifft(self.bh).real

    def _init_etdrk4(self):

        """ This performs pre-computations for the Expotential Time Differencing
            Fourth Order Runge Kutta time stepper. The linear part is calculated
            exactly.
            See Cox and Matthews, J. Comp. Physics., 176(2):430-455, 2002.
                Kassam and Trefethen, IAM J. Sci. Comput., 26(4):1214-233, 2005. """


        #
        # coefficients for q-equation
        #

        # the exponent for the linear part
        self.c = np.zeros((self.nl,self.nk),self.dtype_cplx)
        self.c += -self.wv2*self.nu - self.wv4*self.nu4
        self.c += -self.r
        ch = self.c*self.dt
        self.expch = np.exp(ch)
        self.expch_h = np.exp(ch/2.)
        self.expch2 = np.exp(2.*ch)

        M = 32.  # number of points for line integral in the complex plane
        rho = 1.  # radius for complex integration
        r = rho*np.exp(2j*np.pi*((np.arange(1.,M+1))/M)) # roots for integral
        LR = ch[...,np.newaxis] + r[np.newaxis,np.newaxis,...]
        LR2 = LR*LR
        LR3 = LR2*LR
        self.Qh   =  self.dt*(((np.exp(LR/2.)-1.)/LR).mean(axis=-1))
        self.f0  =  self.dt*( ( ( -4. - LR + ( np.exp(LR)*( 4. - 3.*LR + LR2 ) ) )/ LR3 ).mean(axis=-1) )
        self.fab =  self.dt*( ( ( 2. + LR + np.exp(LR)*( -2. + LR ) )/ LR3 ).mean(axis=-1) )
        self.fc  =  self.dt*( ( ( -4. -3.*LR - LR2 + np.exp(LR)*(4.-LR) )/ LR3 ).mean(axis=-1) )

        #
        # coefficients for b-equation
        #

        # the exponent for the linear part
        self.c = np.zeros((self.nl,self.nk),self.dtype_cplx)
        self.c += -self.wv2*self.kappa - self.wv4*self.kappa4
        self.c += -self.r
        ch = self.c*self.dt
        self.expchb = np.exp(ch)
        self.expch_hb = np.exp(ch/2.)
        self.expch2b = np.exp(2.*ch)

        LR = ch[...,np.newaxis] + r[np.newaxis,np.newaxis,...]
        LR2 = LR*LR
        LR3 = LR2*LR
        self.Qhb   =  self.dt*(((np.exp(LR/2.)-1.)/LR).mean(axis=-1))
        self.f0b  =  self.dt*( ( ( -4. - LR + ( np.exp(LR)*( 4. - 3.*LR + LR2 ) ) )/ LR3 ).mean(axis=-1) )
        self.fabb =  self.dt*( ( ( 2. + LR + np.exp(LR)*( -2. + LR ) )/ LR3 ).mean(axis=-1) )
        self.fcb  =  self.dt*( ( ( -4. -3.*LR - LR2 + np.exp(LR)*(4.-LR) )/ LR3 ).mean(axis=-1) )

    def jacobian_psi_q(self):
        """ Compute the Jacobian between psi and q. Return in Fourier space. """
        self.u, self.v = self.ifft(-self.li*self.ph).real, self.ifft(self.ki*self.ph).real
        self.U, self.V = self.u.mean(axis=1)[:,np.newaxis], self.v.mean(axis=1)[:,np.newaxis]
        self.q = self.ifft(self.qh)
        self.Q = self.q.mean(axis=1)[:,np.newaxis]
        self.up,self.vp, self.qp = self.u-self.U,self.v-self.V,self.q-self.Q
        #return self.ki*self.fft(self.u*self.q) + self.li*self.fft(self.v*self.q)
        Jeeh = self.ki*self.fft(self.up*self.qp)+self.li*self.fft(self.vp*self.qp)
        if self.ql:
            Jee = self.ifft(Jeeh)
            Jeeh = self.fft(Jee.mean(axis=1)[...,np.newaxis]*np.ones((1,self.nx)))

        return  self.ki*self.fft(self.U*self.qp + self.up*self.Q)+ \
                 + self.li*self.fft(self.V*self.qp + self.vp*self.Q) + Jeeh

    def jacobian_psi_b(self):
        """ Compute the Jacobian between psi and q. Return in Fourier space. """
        #self.u, self.v = self.ifft(-self.li*self.ph).real, self.ifft(self.ki*self.ph).real
        self.b = self.ifft(self.bh)
        self.B = self.b.mean(axis=1)[:,np.newaxis]
        self.bp = self.b-self.B
        #return self.ki*self.fft(self.u*self.b) + self.li*self.fft(self.v*self.b)
        Jeeh = self.ki*self.fft(self.up*self.bp)+self.li*self.fft(self.vp*self.bp)
        if self.ql:
            Jee = self.ifft(Jeeh)
            Jeeh = self.fft(Jee.mean(axis=1)[...,np.newaxis]*np.ones((1,self.nx)))

        return  self.ki*self.fft(self.U*self.bp + self.up*self.B)+ \
                 + self.li*self.fft(self.V*self.bp + self.vp*self.B) + Jeeh

    def _invert(self):
        """ From qh compute ph and compute velocity. """
        # invert for psi
        self.ph = -self.wv2i*(self.qh)

    def set_q(self,q):
        """ Initialize pv """
        self.q = q
        self.qh = self.fft(self.q)

    def set_b(self,b):
        """ Initialize pv """
        self.b = b
        self.bh = self.fft(self.b)

    def set_forcing(self,f):
        """ Initialize forcing """
        self.f = f
        self.fh = self.fft(self.f)
        self.fh = self.fh

    def _init_fft(self):
        if self.use_mkl:
            import mkl
            mkl.set_num_threads(self.nthreads)
            import mkl_fft
            self.fft =  (lambda x : mkl_fft.fft2(x))
            self.ifft = (lambda x : mkl_fft.ifft2(x))
        else:
            self.fft =  (lambda x : np.fft.fft2(x))
            self.ifft = (lambda x : np.fft.ifft2(x))

    def _print_status(self):
        """Output some basic stats."""
        self.tc += 1
        self.t += self.dt

        if (self.log_level) and ((self.tc % self.twrite)==0):
            self.ke = self._calc_ke()
            self.pe = self._calc_pe()
            self.cfl = self._calc_cfl()
            self.logger.info('Step: %i, Time: %4.3e, P: %4.3e , KE: %4.3e, PE: %4.3e,E: %4.3e,CFL: %4.3f'
                    , self.tc,self.t, self.t/self.tmax,self.ke,self.pe,self.ke+self.pe,self.cfl)
            assert self.cfl<self.cflmax, self.logger.error('CFL condition violated')

    def _calc_ke(self):
        return 0.5*(self.up**2+self.vp**2).mean()

    def _calc_pe(self):
        return 0.5*(self.bp**2).mean()/self.N2

    def _calc_ke_ave(self):
        return 0.5*(self.U**2).mean()

    def _calc_pe_ave(self):
        return 0.5*(self.B**2).mean()/self.N2

    def _calc_epsilon(self):
        return self.nu*(self.q**2).mean()

    def _calc_epsilon_eddy(self):
        return self.nu*(self.qp**2).mean()

    def _calc_drag_q(self):
        return self.r*(self.spec_var(self.wv*self.ph)).mean()

    def _calc_drag_b(self):
        return self.r*(self.b**2).mean()/self.N2

    def _calc_hepsilon(self):
        return self.nu4*(self.spec_var(self.wv*self.qh))

    def _calc_hepsilon_eddy(self):
        return self.nu4*(self.spec_var(self.wv*self.qph))

    def _calc_hchi(self):
        return self.kappa4*(self.spec_var(self.wv2*self.bh)/self.N2)

    def _calc_hchi_eddy(self):
        return self.kappa4*(self.spec_var(self.wv2*self.bph)/self.N2)

    def _calc_chi(self):
        return self.kappa*(self.spec_var(self.wv*self.bh)/self.N2)

    def _calc_chi_eddy(self):
        return self.kappa*(self.spec_var(self.wv*self.bph)/self.N2)


    def _calc_work(self):
        self.p = self.ifft(self.ph).real
        return -(self.p*self.f).mean()

    def _calc_production(self):
        return -(self.v*self.b).mean()

    def spec_var(self,ph):
        """ compute variance of p from Fourier coefficients ph """
        var_dens = 2. * np.abs(ph)**2 / (self.nx*self.ny)**2
        # only half of coefs [0] and [nx/2+1] due to symmetry in real fft2
        var_dens[:,0],var_dens[:,-1] = var_dens[:,0]/2.,var_dens[:,-1]/2.
        return var_dens.sum()

    ### All the diagnostic stuff follows. ###
    def _calc_cfl(self):
        return np.abs(np.hstack([self.u,self.U, self.v])).max()*self.dt/self.dx

    # saving stuff
    def _init_save_snapshots(self,path):

        self.fno = path

        if not os.path.isdir(self.fno):
            os.makedirs(self.fno)
            os.makedirs(self.fno+"/snapshots/")

    def _file_exist(self, fno):
        if os.path.exists(fno):
            if self.overwrite:
                os.remove(fno)
            else: raise IOError("File exists: {0}".format(fno))

    def _save_setup(self,):

        """Save setup  """

        fno = self.fno + '/setup.h5'

        self._file_exist(fno)

        h5file = h5py.File(fno, 'w')
        h5file.create_dataset("grid/nx", data=(self.nx),dtype=int)
        h5file.create_dataset("grid/x", data=(self.x))
        h5file.create_dataset("grid/y", data=(self.y))
        h5file.create_dataset("grid/wv", data=self.wv)
        h5file.create_dataset("grid/k", data=self.kk)
        h5file.create_dataset("grid/l", data=self.ll)
        h5file.create_dataset("constants/N", data=self.N)
        h5file.create_dataset("constants/nu", data=self.nu)
        h5file.create_dataset("constants/kappa", data=self.kappa)
        h5file.create_dataset("constants/nu4", data=self.nu4)
        h5file.create_dataset("constants/kappa4", data=self.kappa4)

        # h5file.create_dataset("constants/f0", data=(self.f))
        h5file.close()

    def _save_snapshots(self, fields=['t','q','b','U']):

        """ Save snapshots of fields """

        if ( ( not (self.tc%self.tsave_snapshots) ) & (self.save_snapshots) ):

            fno = self.fno + '/snapshots/{:015.0f}'.format(self.t)+'.h5'

            self._file_exist(fno)

            h5file = h5py.File(fno, 'w')

            for field in fields:
                if field == 't':
                    h5file.create_dataset(field, data=(self.t))
                else:
                    h5file.create_dataset(field, data=eval("self."+field))

            h5file.close()
        else:
            pass

    def _save_diagnostics(self):

        """ Save diagnostics """

        fno = self.fno + 'diagnostics.h5'

        self._file_exist(fno)

        h5file = h5py.File(fno, 'w')

        for diagnostic in self.diagnostics_list:
            h5file.create_dataset(diagnostic, data=eval("self."+diagnostic))

        h5file.close()

    def _initialize_diagnostics(self):

        self.diagnostics = dict()

        """Diagnostics common to all models."""

        self.add_diagnostic('time',
                description='Time',
                units='seconds',
                types = 'scalar',
                function = (lambda self: self.t)
        )

        self.add_diagnostic('ke_eddy',
                description='Eddy Kinetic Energy',
                units=r'm^2 s^{-2}',
                types = 'scalar',
                function = (lambda self: self._calc_ke())
        )

        self.add_diagnostic('pe_eddy',
                description='Eddy Potential Energy',
                units=r'm^2 s^{-2}',
                types = 'scalar',
                function = (lambda self: self._calc_pe())
        )

        self.add_diagnostic('ke_mean',
                description='Kinetic Energy of x-averaged flow',
                units=r'm^2 s^{-2}',
                types = 'scalar',
                function = (lambda self: self._calc_ke_ave())
        )

        self.add_diagnostic('pe_mean',
                description='Potential Energy of x-averaged flow',
                units=r'm^2 s^{-2}',
                types = 'scalar',
                function = (lambda self: self._calc_pe_ave())
        )

        self.add_diagnostic('epsilon',
                description='Kinetic energy dissipation',
                units=r'm^2 s^{-3}',
                types = 'scalar',
                function = (lambda self: self._calc_epsilon())
        )

        self.add_diagnostic('epsilon_eddy',
                description='Eddy kinetic energy dissipation',
                units=r'm^2 s^{-3}',
                types = 'scalar',
                function = (lambda self: self._calc_epsilon_eddy())
        )

        self.add_diagnostic('hepsilon',
                description='Kinetic energy dissipation',
                units=r'm^2 s^{-3}',
                types = 'scalar',
                function = (lambda self: self._calc_hepsilon())
        )

        self.add_diagnostic('hepsilon_eddy',
                description='Eddy kinetic energy dissipation',
                units=r'm^2 s^{-3}',
                types = 'scalar',
                function = (lambda self: self._calc_hepsilon_eddy())
        )


        self.add_diagnostic('chi',
                description='Potential energy dissipation',
                units=r'm^2 s^{-3}',
                types = 'scalar',
                function = (lambda self: self._calc_chi())
        )

        self.add_diagnostic('chi_eddy',
                description='Eddy potential energy dissipation',
                units=r'm^2 s^{-3}',
                types = 'scalar',
                function = (lambda self: self._calc_chi_eddy())
        )

        self.add_diagnostic('hchi',
                description='Potential energy dissipation',
                units=r'm^2 s^{-3}',
                types = 'scalar',
                function = (lambda self: self._calc_hchi())
        )

        self.add_diagnostic('hchi_eddy',
                description='Eddy potential energy dissipation',
                units=r'm^2 s^{-3}',
                types = 'scalar',
                function = (lambda self: self._calc_hchi_eddy())
        )

        self.add_diagnostic('work',
                        description='Work by f_u',
                        units=r'm^2 s^{-3}',
                        types = 'scalar',
                        function = (lambda self: self._calc_work())
        )

        self.add_diagnostic('production',
                        description='Potential energy production',
                        units=r'm^2 s^{-3}',
                        types = 'scalar',
                        function = (lambda self: self._calc_production())
        )

        self.add_diagnostic('drag_q',
                        description='Linear drag of q',
                        units=r'm^2 s^{-3}',
                        types = 'scalar',
                        function = (lambda self: self._calc_drag_q())
        )

        self.add_diagnostic('drag_b',
                        description='Linear drag of b',
                        units=r'm^2 s^{-3}',
                        types = 'scalar',
                        function = (lambda self: self._calc_drag_b())
        )

    def _calc_derived_fields(self):
        """Should be implemented by subclass."""
        #self._calc_energy_conversion()
        self.qp = self.q - self.q.mean(axis=1)[:,np.newaxis]
        self.qph = self.fft(self.qp)

        self.bp = self.b - self.b.mean(axis=1)[:,np.newaxis]
        self.bph = self.fft(self.bp)

    def _set_active_diagnostics(self, diagnostics_list):
        for d in self.diagnostics:
            self.diagnostics[d]['active'] == (d in diagnostics_list)

    def add_diagnostic(self, diag_name, description=None, units=None,types='scalar', function=None):

        assert hasattr(function, '__call__')
        assert isinstance(diag_name, str)

        self.diagnostics[diag_name] = {
           'description': description,
           'units': units,
           'active': True,
           'count': 0,
           'type': types,
           'function': function,}

    def describe_diagnostics(self):
        """Print a human-readable summary of the available diagnostics."""
        diag_names = self.diagnostics.keys()
        diag_names.sort()
        print('NAME               | DESCRIPTION')
        print(80*'-')
        for k in diag_names:
            d = self.diagnostics[k]
            print('{:<10} | {:<54}').format(
                 *(k,  d['description']))

    def _increment_diagnostics(self):

        if  ( not (self.tc%self.tdiags) ):
            self._calc_derived_fields()

            for dname in self.diagnostics:
                res = self.diagnostics[dname]['function'](self)
                try:
                    if self.diagnostics[dname]['type'] == 'scalar':
                        self.diagnostics[dname]['value'] = np.hstack([ self.diagnostics[dname]['value'],res])
                    else:
                        self.diagnostics[dname]['value'] += res
                        self.diagnostics[dname]['value'] *= 0.5
                except:
                    if self.diagnostics[dname]['type'] == 'scalar':
                        self.diagnostics[dname]['value'] = np.array(res)
                    else:
                        self.diagnostics[dname]['value'] = res


    def get_diagnostic(self, dname):
        return (self.diagnostics[dname]['value'])
