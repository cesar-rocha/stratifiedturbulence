from numpy import pi, infty

path = "./new/ql_filt"

L  = 2*pi      # Domain size
nx = 1024      # Number of modes
m0 = 3*2*pi/L  # Wavenumber of the forcing
Fr = 0.02      # Froude number
Re4 = 25000    # Modified Reynolds number (for 4th-order hyperviscosity)
ql =  True     # Quasi-linear flag
tN = 2*pi/1    # Stratification time-scale
dt = 3.e-4*tN  # Numerical time step
tmax = 50*tN   # Params of simulation

