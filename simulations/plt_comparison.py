import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['lines.linewidth'] = 2.5

ap = .65
plt.style.use('seaborn-deep')

plt.close('all')


nl = np.load("new/nl/diags.npz")
ql = np.load("new/ql_filt/diags.npz")

tN = nl['tN']
U0 = 1.
s  = U0**2/2


plt.figure(figsize=(10,8))

plt.subplot(321)

plt.plot(nl['time']/tN,nl['energy']/s,label=r'Non-linear',alpha=ap)
plt.plot(ql['time']/tN,ql['energy']/s,label=r'Quasi-linear',alpha=ap)

plt.ylim(0,4.75)
plt.xlim(0,1000)
plt.ylabel(r'Energy $[E\times  2\pi/(N E_0)]$')
plt.xticks([])
plt.text(50,4.35,'Total energy')
plt.legend(loc=(1.135,.5),ncol=2,frameon=True)

plt.subplot(323)
plt.plot(nl['time']/tN,nl['mke']/s,label=r'$\overline{K}$, non-linear',alpha=ap)
plt.plot(ql['time']/tN,ql['mke']/s,label=r'$\overline{K}$, quasi-linear',alpha=ap)
plt.ylim(0,2.75)
plt.xlim(0,1000)
plt.ylabel(r'Energy $[E\times  2\pi/(N E_0)]$')
plt.xticks([])
plt.text(35,2.55,'Kinetic energy, x-average')

plt.subplot(325)
plt.plot(nl['time']/tN,nl['mpe']/s,label=r'$\overline{P}$, non-linear',alpha=ap)
plt.plot(ql['time']/tN,ql['mpe']/s,label=r'$\overline{P}$, quasi-linear',alpha=ap)
plt.ylim(0,.55)
plt.xlim(0,1000)
plt.ylabel(r'Energy $[E\times  2\pi/(N E_0)]$')
plt.xlabel(r'Time $[t\times N/2\pi]$')
plt.text(32,.5,'Potential energy, x-average')

plt.subplot(324)
plt.plot(nl['time']/tN,nl['eke']/s,label=r"$K'$, non-linear",alpha=ap)
plt.plot(ql['time']/tN,ql['eke']/s,label=r"$K'$, quasi-linea",alpha=ap)
plt.ylim(0,2.)
plt.xlim(0,1000)
plt.yticks([])
plt.xticks([])
plt.text(42,1.55,'Kinetic energy, eddy')

plt.subplot(326)
plt.plot(nl['time']/tN,nl['epe']/s,label=r"$P'$, non-linear",alpha=ap)
plt.plot(ql['time']/tN,ql['epe']/s,label=r"$P'$, quasi-linea",alpha=ap)
plt.ylim(0,2.)
plt.xlim(0,1000)
plt.yticks([])
plt.xlabel(r'Time $[t\times N/2\pi]$')
plt.text(45,1.55,'Potential energy, eddy')

plt.savefig("128/control/energy_comparison.pdf",bbox_inches='tight')


ep_nl = nl['hep'] + nl['ep']
chi_nl = nl['hchi'] + nl['chi']

ep_ql = ql['hep'] + ql['ep']*0  # check this
chi_ql = ql['hchi'] + ql['chi']*0


plt.figure(figsize=(10,4))
s  = U0**2/2/tN

plt.subplot(121)

plt.plot(nl['time']/tN,nl['work']/s,label=r'$W$',alpha=ap)
plt.plot(nl['time']/tN,-ep_nl/s,label=r'$-\varepsilon$',alpha=ap)
plt.plot(nl['time']/tN,-chi_nl/s,label=r'$-\chi$',alpha=ap)
plt.plot(nl['time']/tN,nl['denergy']/s,color='y',label=r'$\dot{E}$',alpha=ap)
plt.plot(nl['time']/tN,nl['rhs']/s,'k--',linewidth=1.5,
        label=r'$W - \varepsilon -\chi$',alpha=ap)
plt.ylim(-.35,.35)
plt.xlim(0,1000)
plt.legend(loc=(0.2,1.025),ncol=5,frameon=True)
#plt.grid()
plt.text(70,0.3,'Non-linear')
plt.xlabel(r'Time $[t\times N/2\pi]$')
plt.ylabel(r'Power $[\dot{E}\times  2\pi/(N E_0)]$')

plt.subplot(122)
plt.plot(ql['time']/tN,ql['work']/s,label=r'$W$',alpha=ap)
plt.plot(ql['time']/tN,-ep_ql/s,label=r'$-\varepsilon$',alpha=ap)
plt.plot(ql['time']/tN,-chi_ql/s,label=r'$-\chi$',alpha=ap)
plt.plot(ql['time']/tN,ql['denergy']/s,color='y',label=r'$\dot{E}$',alpha=ap)
plt.plot(ql['time']/tN,ql['rhs']/s,'k--',linewidth=1.5,
        label=r'$W - \varepsilon -\chi$',alpha=ap)
plt.xlim(0,1000)
plt.text(70,0.3,'Quasi-linear')
#plt.legend(loc=4,ncol=3)
#plt.grid()
plt.xlabel(r'Time $[t\times N/2\pi]$')
plt.yticks([])

plt.ylim(-.35,.35)
plt.savefig("128/control/energy_budget_comparison.pdf",bbox_inches='tight')
