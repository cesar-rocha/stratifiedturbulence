import matplotlib.pyplot as plt
plt.rcParams['contour.negative_linestyle'] = 'dashed'

import numpy as np
from numpy import pi
import scipy.special as special

import cmocean

import model as model
from InitialConditions import *

#
# PARAMETERS
#

from params import *

# forcing
N = 1      # have to change this to be dependent on Froude
F = Fr*N   

# dissipation
nu4 = (Fr*N)/Re4/(m0**4)

if Re4 == infty:
    use_filter = True
else:
    use_filter = False

# parameters
U0 = F/m0

# seed for IC
sig = 1e-4

#
# SET MODEL python CLASS
#

m = model.Model(Lx=L,Lz=L,nx=nx, tmax = tmax,dt = dt,N=N, twrite=50,
                nu=0,kappa=0,nu4=nu4,kappa4=nu4,r=0,use_filter=use_filter,
                save_snapshots=True,
                tsave_snapshots=100,path=path,ql=ql, use_mkl=True, nthreads=12)

#
# SET INITIAL CONDITIONS: inifitesimal random vorticity and buoyancy fields
#                           

q = McWilliams1984(m, k0= 7*2*np.pi/L, E=U0**2/2)
q -= q.mean(axis=1)[...,np.newaxis]

b = np.random.rand(m.nx,m.ny)
b -= b.mean(axis=1)[...,np.newaxis]

fq = F*np.sin(m0*m.y)
m.set_forcing(fq)

U = U0*np.sin(m0*m.y[...,0])
Q = -U0*m0*np.cos(m0*m.y)
B = sig*(np.random.rand(U.size) - 0.5)

m.set_q(sig*q)
m.set_b(sig*b)

#
# RUN the MODEL
#

m.run()

#
# GET and SAVE DIAGNOSTICS
#

eke = m.get_diagnostic('ke_eddy')
epe = m.get_diagnostic('pe_eddy')
mke = m.get_diagnostic('ke_mean')
mpe = m.get_diagnostic('pe_mean')
dragq = m.get_diagnostic('drag_q')
dragb = m.get_diagnostic('drag_b')
ep = m.get_diagnostic('epsilon')
chi = m.get_diagnostic('chi')
work = m.get_diagnostic('work')
hep = m.get_diagnostic('hepsilon')
hchi = m.get_diagnostic('hchi')
prod = m.get_diagnostic('production')

energy = eke+epe+mke+mpe
time = m.get_diagnostic('time')

#rhs = work-ep-chi-hep-hchi-dragq-dragb
rhs = work-ep-chi-hep-hchi

dt = time[1]-time[0]
denergy = np.gradient(energy,dt)
res = denergy-work+ep+chi+hep+hchi
z = m.y[...,0]

#B = m.B + (N**2)*z
B = m.b.mean(axis=1) + (N**2)*z

np.savez(path+"/diags.npz",time=time,eke=eke,epe=epe,mke=mke,ep=ep,chi=chi,
        work=work,hep=hep,hchi=hchi,prod=prod,energy=energy,rhs=rhs,
        denergy=denergy,tN=tN,U0=U0,mpe=mpe)
